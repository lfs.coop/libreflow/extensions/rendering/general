import os
import subprocess
import kabaret.app.resources as resources
from kabaret import flow
from kabaret.flow_contextual_dict import ContextualView, get_contextual_dict
from libreflow.baseflow.users import PresetValue, PresetSessionValue, PresetChoiceValue
from libreflow.baseflow.file import OpenWithBlenderAction
from libreflow.flows.default.flow.file import TrackedFile
from libreflow.utils.b3d import wrap_python_expr
from libreflow.resources.icons import gui as _


class RevisionsChoiceValue(flow.values.ChoiceValue):

    STRICT_CHOICES = False

    _file = flow.Parent(2)

    def choices(self):
        return self._file.get_revision_names(sync_status='Available', published_only=True)

    def revert_to_default(self):
        if self._file.is_empty():
            self.set('')
            return

        revision = self._file.get_head_revision(sync_status='Available')
        revision_name = ''
        
        if revision is None:
            choices = self.choices()
            if choices:
                revision_name = choices[0]
        else:
            revision_name = revision.name()
        
        self.set(revision_name)
    
    def _fill_ui(self, ui):
        super(RevisionsChoiceValue, self)._fill_ui(ui)
        ui['hidden'] = self._file.is_empty(on_current_site=True)


class ResolutionChoiceValue(PresetChoiceValue):

    DEFAULT_EDITOR = 'choice'

    def choices(self):
        return ['25', '50', '100']


class RenderBlenderPlayblast(OpenWithBlenderAction):

    revision_name = flow.Param("", RevisionsChoiceValue).watched()
    resolution_percentage = flow.Param('100', ResolutionChoiceValue).ui(
        label='Resolution scale (%)'
    )

    _files = flow.Parent(2)
    _task = flow.Parent(3)

    with flow.group('Advanced'):
        reduce_textures = flow.SessionParam(False, PresetSessionValue).ui(
            tooltip="Reduce texture sizes before render, to reduce memory footprint",
            editor='bool',
        )
        target_texture_width = flow.SessionParam(4096, PresetSessionValue).ui(
            tooltip="Size to reduce textures to",
            editor='int',
        )

    def get_run_label(self):
        return 'Render playblast march'

    def _sequence_number_from_name(self, sequence_name):
        tmp = re.findall(r"\d+", sequence_name)
        numbers = list(map(int, tmp))
        return numbers[0] if numbers else -999
    
    def check_default_values(self):
        self.revision_name.revert_to_default()
        self.resolution_percentage.apply_preset()
        self.reduce_textures.apply_preset()
        self.target_texture_width.apply_preset()
    
    def update_presets(self):
        self.resolution_percentage.update_preset()
        self.reduce_textures.update_preset()
        self.target_texture_width.update_preset()

    def needs_dialog(self):
        msg = self.runner_configured()
        if msg is not None:
            name, _ = self.runner_name_and_tags()
            error_txt = '<div style="color: red">Error:</div>'
            self.root().session().log_error(msg)
            self.message.set((f'<h3 style="font-weight: 400">{error_txt} '
                f'Application {name} not configured (see terminal).'))
            self._buttons.set(['Cancel'])
        else:
            self.check_default_values()
            buttons = ['Render', 'Cancel']
            
            if self.root().project().get_current_site().site_type.get() == 'Studio':
                buttons.insert(1, 'Submit job')
            self._buttons.set(buttons)
            
        return True

    def allow_context(self, context):
        return (
            super(RenderBlenderPlayblast, self).allow_context(context)
            and not self._file.is_empty()
        )
    
    def playblast_infos_from_revision(self, revision_name):
        filepath = self._file.path.get()
        filename = "_".join(self._file.name().split("_")[:-1])

        # Check if there is a AE compositing file
        if self._files.has_file('compositing', "aep"):
            playblast_filename = filename + "_movie_blend"
            playblast_revision_filename = self._file.complete_name.get() + "_movie_blend.mov"
        else:
            playblast_filename = filename + "_movie"
            playblast_revision_filename = self._file.complete_name.get() + "_movie.mov"
        
        playblast_filepath = os.path.join(
            self.root().project().get_root(),
            os.path.dirname(filepath),
            playblast_filename + "_mov",
            revision_name,
            playblast_revision_filename
        )

        return playblast_filepath, playblast_filename, self._file.path_format.get()

    def child_value_changed(self, child_value):
        if child_value is self.revision_name:
            msg = "<h2>Render playblast</h2>"
            playblast_path, _, _ = self.playblast_infos_from_revision(child_value.get())

            # Check if revision playblast exists
            if os.path.exists(playblast_path):
                msg += (
                    "<font color=#D50000>"
                    "Choosing to render a revision's playblast "
                    "will override the existing one."
                    "</font>"
                )

            self.message.set(msg)

    def extra_argv(self):
        file_settings = get_contextual_dict(
            self._file, "settings", ["sequence", "shot"]
        )
        project_name = self.root().project().name()
        revision = self._file.get_revision(self.revision_name.get())
        do_render = False
        python_expr = """import bpy
bpy.ops.lfs.playblast(do_render=%s, filepath='%s', studio='%s', project='%s', sequence='%s', scene='%s', quality='%s', do_reduce_textures=%s, target_texture_width=%s, resolution_percentage=%s, version='%s', template_path='%s')""" % (
            str(do_render),
            self.output_path,
            self.root().project().get_current_site().name(),
            project_name,
            file_settings.get("sequence", "undefined"),
            file_settings.get("shot", "undefined"),
            'PREVIEW',
            str(self.reduce_textures.get()),
            str(self.target_texture_width.get()),
            self.resolution_percentage.get(),
            self.revision_name.get(),
            resources.get('mark_sequence.fields', 'compositing.json').replace('\\', '/'),
        )

        if not do_render:
            python_expr += "\nbpy.ops.wm.quit_blender()"
        
        args = [
            revision.get_path(),
            "--addons",
            "mark_sequence",
            "--python-expr",
            wrap_python_expr(python_expr),
        ]

        if do_render:
            args.insert(0, '-b')
        
        return args

    def run(self, button):
        if button == "Cancel":
            return
        elif button == "Submit job":
            self.update_presets()

            submit_action = self._file.submit_blender_playblast_job
            submit_action.revision_name.set(self.revision_name.get())
            submit_action.resolution_percentage.set(self.resolution_percentage.get())
            submit_action.reduce_textures.set(self.reduce_textures.get())
            submit_action.target_texture_width.set(self.target_texture_width.get())
            
            return self.get_result(
                next_action=submit_action.oid()
            )
        
        self.update_presets()

        revision_name = self.revision_name.get()
        playblast_path, playblast_name, path_format = self.playblast_infos_from_revision(
            revision_name
        )

        # Get or create playblast file
        if not self._files.has_file(playblast_name, "mov"):
            tm = self.root().project().get_task_manager()
            df = next((
                file_data for file_name, file_data in tm.get_task_files(self._task.name()).items()
                if file_data[0] == f'{playblast_name}.mov'), None
            )
            playblast_file = self._files.add_file(
                name=playblast_name,
                extension="mov",
                tracked=True,
                default_path_format=path_format if df is None else df[1]
            )
            playblast_file.file_type.set('Outputs')
        else:
            playblast_file = self._files[playblast_name + "_mov"]
        
        # Get or add playblast revision
        if playblast_file.has_revision(revision_name):
            playblast_revision = playblast_file.get_revision(
                revision_name
            )
        else:
            source_revision = self._file.get_revision(revision_name)
            playblast_revision = playblast_file.add_revision(
                name=revision_name,
                comment=source_revision.comment.get()
            )
        
        # Configure playblast revision
        playblast_revision.set_sync_status("Available")

        # Store revision path as playblast output path
        self.output_path = playblast_revision.get_path().replace("\\", "/")
        
        # Ensure playblast revision folder exists and is empty
        if not os.path.exists(self.output_path):
            os.makedirs(os.path.dirname(self.output_path), exist_ok=True)
        else:
            os.remove(self.output_path)

        result = super(RenderBlenderPlayblast, self).run(button)
        self._files.touch()
        return result


def render_blender_playblast(parent):
    if type(parent) is TrackedFile and parent.name().endswith('blend'):
        r = flow.Child(RenderBlenderPlayblast).ui(label='Render playblast')
        r.name = 'render_blender_playblast'
        r.index = 29
        return r


def install_extensions(session): 
    return {
        "playblast": [
            render_blender_playblast,
        ],
    }



# def get_handlers():
#     return [
#         ("^/[^/]+/films/[^/]+/sequences/sq\d+\D?/shots/sh\d+\D?/tasks/[^/]+/files/[^/]+_render(_nocam)?$", "render_master_movie", classqualname(RenderMasterMovie), {}),
#         ("^/[^/]+/films/[^/]+/sequences/sq\d+\D?/shots/sh\d+\D?$", "render_master_movie", classqualname(RenderShotMasterMovie), {}),
#         ("^/[^/]+/films/[^/]+/sequences/sq\d+\D?$", "render_master_movies", classqualname(RenderSequenceMasterMovies), {})
#     ]
